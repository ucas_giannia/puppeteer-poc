const faker = require('faker');
const puppeteer = require('puppeteer');

const appUrlBase = 'https://togo-digital.ucasenvironments.com'
const routes = {
  public: {
    login: `${appUrlBase}/account/login`,
  },
  private: {
    search: `${appUrlBase}/search`,
    dashboard: `${appUrlBase}/search/dashboard`,
    profile: `${appUrlBase}/learner/profile`,
    contactdetails: `${appUrlBase}/learner/profile/contact-details`,
  },
}

const user = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  mobile: faker.phone.phoneNumberFormat(),
}

let browser
let page
beforeAll(async () => {
  browser = await puppeteer.launch(
    process.env.DEBUG
      ? {
        headless: false,
        }
      : {
        headless: false,
        setViewport: ({width: 0, height: 0}),
      }
  )
  page = await browser.newPage()
})

describe('Application processes', () => {
  test('can apply', async () => {
    jest.setTimeout(100000);
    await page.goto(routes.public.login);
    await page.type('#Email', 'bbtsuperuser@ucas.ac.uk');
    await page.type('#Password', 'F1ndMyBugz!');
    await page.click('#LoginSubmit');
    await page.waitForSelector('#dashboard > div > div > div:nth-child(1) > div > div > h1');
    await page.goto(routes.private.dashboard);
    await page.click('a[data-tab="postgraduates"]');
    await page.click('a[href="/learner/dashboard"]');
    await page.waitForNavigation();
    await page.waitForNavigation();
    await page.click('a[class="button button--small button--primary spinner"]');
    await page.type('#SearchText', 'Edge Hill');
    await page.select('#StudyLevel', 'Destination_Postgraduate')
    await page.select('#EntryYear', '2019')
    await page.click('#GlobalSearchSubmit');
    await page.waitForNavigation();
    await page.click('label[for="Scheme_UCAS_Postgraduate_checkbox"]');
    await page.click('div[class="course-details"]');
    await page.title('Advanced Computer Networking at Edge Hill University - UCAS');
    await page.click('li[class="list-item--course-option js-course-option-count "]');
    await page.click('a[class="button--grow js-applicationServices-redirect button icon--chevron-right-dark icon-inline--right"]');
    await page.waitForSelector('span[class="page-title--secondary"]');
  })
})

describe('Searching for a course', () => {
  test('can add course as a favourite', async () => {
    jest.setTimeout(100000);
    await page.goto(routes.public.login);
    await page.type('#Email', 'bbtsuperuser@ucas.ac.uk');
    await page.type('#Password', 'F1ndMyBugz!');
    await page.click('#LoginSubmit');
    await page.waitForSelector('#dashboard > div > div > div:nth-child(1) > div > div > h1');
    await page.goto(routes.private.dashboard);
    await page.click('a[data-tab="postgraduates"]');
    await page.click('a[href="/learner/dashboard"]');
    await page.waitForNavigation();
    await page.waitForNavigation();
    await page.click('a[class="button button--small button--primary spinner"]');
    await page.type('#SearchText', 'Edge Hill');
    await page.select('#StudyLevel', 'Destination_Postgraduate')
    await page.select('#EntryYear', '2019')
    await page.click('#GlobalSearchSubmit');
    await page.waitForNavigation();
    await page.click('label[for="Scheme_UCAS_Postgraduate_checkbox"]');
    await page.click('button[class="search-result__favourite js-shortlist-add"]');
    await page.goto(routes.private.dashboard);
    await page.click('a[data-tab="mycourses"]');
    await page.waitForSelector('h3[class="brick__title"]');
    await page.click('a[class="search-result__favourite search-result__favourite--chosen js-shortlist-remove"]');
    await page.click('button[class="button button--primary js-shortlist-remove-confirm"]');
    await page.waitForSelector('p[class="paragraph-highlight"]');
  })
    test('verify user can select multiple locations', async () => {
    jest.setTimeout(100000);
    await page.goto(routes.public.login);
    await page.type('#Email', 'bbtsuperuser@ucas.ac.uk');
    await page.type('#Password', 'F1ndMyBugz!');
    await page.click('#LoginSubmit');
    await page.waitForSelector('#dashboard > div > div > div:nth-child(1) > div > div > h1');
    await page.goto(routes.private.dashboard);
    await page.click('a[data-tab="postgraduates"]');
    await page.click('a[href="/learner/dashboard"]');
    await page.waitForNavigation();
    await page.waitForNavigation();
    await page.click('a[class="button button--small button--primary spinner"]');
    await page.type('#SearchText', 'Edge Hill');
    await page.select('#StudyLevel', 'Destination_Postgraduate')
    await page.select('#EntryYear', '2019')
    await page.click('#GlobalSearchSubmit');
    await page.waitForNavigation();
    await page.click('#dynamic-sidebar-open-button');
    await page.click('#Location > div > div > div.Location-filter.display-filter__options > div > div > div:nth-child(1) > label');
    await page.waitForSelector('span[class="course-count"]');
  })
})

afterAll(() => {
    if (!process.env.DEBUG) {
    browser.close()
  }
}) 