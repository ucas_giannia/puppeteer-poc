const faker = require('faker');
const puppeteer = require('puppeteer');

const appUrlBase = 'https://togo-digital.ucasenvironments.com'
const routes = {
  public: {
    login: `${appUrlBase}/account/login`,
  },
  private: {
    search: `${appUrlBase}/search`,
    profile: `${appUrlBase}/learner/profile`,
    contactdetails: `${appUrlBase}/learner/profile/contact-details`,
  },
}

const user = {
  email: faker.internet.email(),
  password: faker.internet.password(),
  mobile: faker.phone.phoneNumberFormat(),
}

let browser
let page
beforeAll(async () => {
  browser = await puppeteer.launch(
    process.env.DEBUG
      ? {
          headless: false,
          slowMo: 100,
        }
      : {
        headless: false,
        setViewport: ({width: 1280, height: 800}),
      }
  )
  page = await browser.newPage()
})

describe('Logging in', () => {
  test('cannot log in due to incorrect credentials', async () => {
    await page.goto(routes.public.login);
    await page.type('#Email', user.email);
    await page.type('#Password', user.password);
    await page.click('#LoginSubmit');
    await page.waitForSelector('#page-validation-error');
  })
  test('can log in', async () => {
    await page.goto(routes.public.login);
    await page.type('#Email', 'bbtsuperuser@ucas.ac.uk');
    await page.type('#Password', 'F1ndMyBugz!');
    await page.click('#LoginSubmit');
    await page.waitForSelector('#dashboard > div > div > div:nth-child(1) > div > div > h1');
  })
})

describe('Application processes', () => {
  test('can view learner profile', async () => {
    await page.goto(routes.private.profile);
    await page.waitForSelector('p[class="impact--medium"]');
  })
  test('can edit contact details', async () => {
    await page.goto(routes.private.contactdetails);
    const input = await page.$('#mobilePhoneNumber');
    await input.click({ clickCount: 3 })
    await page.type('#mobilePhoneNumber', user.mobile);
    await page.click('button[type="Submit"]');
    await page.waitForSelector('div[class="toast-message toast--success"]');
  })
  test('can apply', async () => {
    jest.setTimeout(100000);
    await page.goto(routes.private.search);
    await page.type('#SearchText', 'Cambridge');
    await page.keyboard.press('Enter');
    await page.waitForNavigation();
    await page.click('div[class="course-details"]');
    await page.click('li[class="list-item--course-option js-course-option-count "]');
    await page.click('a[class="button--grow js-applicationServices-redirect button icon--chevron-right-dark icon-inline--right"]');
    await page.waitForNavigation();
    await page.click('label[for="entryPoint_1"]');
    await page.click('button[df-button="primary"]');
    await page.waitForSelector('span[class="page-title--secondary"]');
  })
})

afterAll(() => {
  if (!process.env.DEBUG) {
    browser.close()
  }
})